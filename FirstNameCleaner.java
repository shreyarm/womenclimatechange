import java.io.*;
import java.util.Scanner;

// takes one argument which is a text document listing other CLEANED(sorted in alphabetical order, "name" in first row, author names only)
// csv files (though leave off the csv extension in the list); auto outputs, don't worry about redirecting from stdout
public class FirstNameCleaner {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream(args[0]);
        Scanner scan1 = new Scanner(fis);

        while (scan1.hasNextLine()) {
            String name = scan1.nextLine();
            FileInputStream currentInput = new FileInputStream(name + ".csv");
            DataOutputStream currentOutput = new DataOutputStream(new FileOutputStream("FINAL" + name + ".csv"));

            Scanner scan2 = new Scanner(currentInput);
            while (scan2.hasNextLine()) {
                String line = scan2.nextLine();
                currentOutput.writeBytes(line.replace("\"","").split(" ")[0] + "\n");
            }

            currentOutput.close();
            scan2.close();
            currentInput.close();
        }
        scan1.close();
        fis.close();
    }
}
