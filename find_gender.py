import pandas as pd


# Load data from Excel sheet
# take in list, return total male, total female, total outliers (<0.8)
def find_gen(names):
    male = 0
    female = 0
    outliers = 0
    not_in_data = 0
    unlisted = []

    data = pd.read_csv('cleaned_data (2).csv')
    name_list = data['Name'].to_list()
    gender_list = data['Sex'].to_list()
    prob_list = data['Likelihood']
    for i in range(len(names)):
        try:
            ind = name_list.index(names[i])
        except:
            # if there is no index of name, then name isnt in database
            print("Name \'" + names[i] + "\' not in database" , file=open('output.txt', 'a'))
            unlisted.append(names[i])
            not_in_data+=1
        else:
            # if the name is not 80% probably of sex, we assume its an outlier
            if (prob_list[ind]) < 0.8:
                print('\''+ names[i] +'\' is an Outlier' , file=open('output.txt', 'a'))
                outliers+=1
            else:
                gender = 'Male' if gender_list[ind] == 1 else 'Female'
                if gender == 'Male':
                    male+=1
                else:
                    female+=1
                print("\'"+names[i] +"\' is a " + gender , file=open('output.txt', 'a'))
    print('Total Males: '+ str(male) , file=open('output.txt', 'a'))
    print('Total Females: '+ str(female) , file=open('output.txt', 'a'))
    print('Total Outliers: '+ str(outliers) , file=open('output.txt', 'a'))
    print('Total Names not in Database: '+ str(not_in_data) + ' ---- ' + str(unlisted) , file=open('output.txt', 'a'))
    print('Total Names tested: ' + str(male+female+outliers+not_in_data) , file=open('output.txt', 'a'))

if __name__ == '__main__':
    #with open('input.txt', 'r') as file:
     #   data = file.read().split('\n')
    #print(data)
 #   names = ['Adamsota', 'Seth']
  #  data = pd.read_excel('US_Cleaned_Data.xlsx')
   # names = data['Name'].to_list()
    csv_data = pd.read_csv('unitedstates_FINAL_20182022.csv')
    name_list = csv_data['name'].to_list()
    find_gen(name_list)
