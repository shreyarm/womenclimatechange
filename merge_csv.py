from glob import glob
import pandas as pd 
import os
import math
from pathlib import Path
#code to concatenate two files
my_file = Path("cleaned_data.csv")
if my_file.is_file():  
    os.remove("cleaned_data.csv")
files = glob("*.csv")
df_list = []
for i in range(len(files)):
    df_list.append(pd.read_csv(files[i]))
#(pd.concat(df_list).drop_duplicates(subset=['Name', 'Sex'])).to_csv('compiled_cleaned_dataset.csv')

#df = pd.concat(df_list).drop_duplicates(subset=['Name', 'Sex'])
df = pd.concat(df_list)
df.to_csv('cleaned_data.csv')
names = df['Name'].to_list()
names = list(dict.fromkeys(names))
df.drop_duplicates(subset='Name',inplace=True, keep='first')
        
df.to_csv('cleaned_data.csv')


